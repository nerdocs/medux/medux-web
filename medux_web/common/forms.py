from django import forms
from django.core import validators
from django.utils.translation import gettext_lazy as _


class ContactForm(forms.Form):
    name = forms.CharField(max_length=200, required=True)
    email = forms.EmailField(
        help_text=_("Please enter a valid e-mail address so we can get back to you"),
        required=True,
    )
    title = forms.CharField(max_length=300, required=True)
    message = forms.CharField(widget=forms.Textarea, required=True)
    feedback = forms.CharField(
        widget=forms.HiddenInput,  # ----Botcatcher ----#
        required=False,
        validators=[validators.MaxLengthValidator(0)],
    )
