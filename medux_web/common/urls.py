from django.urls import path
from django.views.generic import TemplateView
from .views import ContactView


app_name = "common"
urlpatterns = [

    path(
        "physio/",
        TemplateView.as_view(template_name="physio.html"),
        name="physio",
    ),

    path(
        "datenschutz/",
        TemplateView.as_view(template_name="datenschutz.html"),
        name="datenschutz",
    ),
    path(
        "contact-sent/",
        TemplateView.as_view(template_name="contactsent.html"),
        name="contact_sent",
    ),
    path(
        "order-sent/",
        TemplateView.as_view(template_name="ordersent.html"),
        name="thankyou",
    ),
    path("contact/",
         ContactView.as_view(template_name="contactus.html"),
         name="contact"),
]
