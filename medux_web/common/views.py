from django.views.generic import FormView, ListView
from .forms import ContactForm
from django.urls import reverse_lazy
from django.shortcuts import render



class ContactView(FormView):
    template_name = "common/contactus.html"
    form_class = ContactForm
    success_url = '/contact-sent/'

