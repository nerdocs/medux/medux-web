from django.contrib import admin
from .models import *
# Register your models here.


class MedicationInline(admin.StackedInline):
    model = Medication
    extra = 1


class PatientAdmin(admin.ModelAdmin):
    inlines = [MedicationInline]

admin.site.register(Patient, PatientAdmin)
admin.site.register(Medication)