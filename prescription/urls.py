
from django.urls import path
from . import views

app_name = "prescription"

urlpatterns = [
    path('', views.MedAddView.as_view(), name="prescriptionform"),
]