from django import forms
from django.forms import modelformset_factory
from django.forms import inlineformset_factory
from .models import *
from django.utils.translation import gettext_lazy as _
from django.core import validators

DELIVER_CHOICES =(
    ("1", "Book it on my e-Card"),
    ("2", "Get it in the office"),
)

class PatientForm(forms.ModelForm):
    # Botcatcher
    feedback = forms.CharField(
        widget=forms.HiddenInput,
        required=False,
        validators=[validators.MaxLengthValidator(0)],
    )

    # preferred_delivery = forms.ChoiceField(choices=DELIVER_CHOICES,
    #     widget=forms.RadioSelect,
    #     required=True,
    # )

    class Meta:
        model = Patient
        fields = "__all__"


class MedForm(forms.ModelForm):
    class Meta:
        model = Medication
        fields = "__all__"
        widgets = {
            "status_closed": forms.CheckboxInput(),
            "med_deliver": forms.RadioSelect(),
            # "med_name": forms.TextInput(attrs={
            #     'placeholder': _('Drug Name')
            # }),
            # "med_dosage": forms.FloatField(attrs={
            #     'placeholder': _('Dosage')
            # }),
            # "med_size": forms.IntegerField(attrs={
            #     'placeholder': _('Package Size')
            # }),
            # "med_amount": forms.IntegerField(attrs={
            #     'placeholder': _('Amount')
            # }),
        }


MedModelFormset = modelformset_factory(
    Medication,
    fields="__all__",
    # extra=2,
    widgets={
        "status_closed": forms.CheckboxInput(),
        "med_deliver": forms.RadioSelect(),
        "med_name": forms.TextInput(attrs={
            'placeholder': _('Drug Name')
        })
    }
)

# MedFormSet = inlineformset_factory(
#     Patient,
#     Medication,
#     fields=('med_name',))
# patient = Patient.objects.all()
# formset = MedFormSet(instance=patient)
