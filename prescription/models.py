from django.db import models
from django.utils.translation import gettext_lazy as _
# Create your models here.


class Patient(models.Model):
    patient_id = models.PositiveIntegerField(verbose_name=_("Patient ID"))
    pat_pwd = models.CharField(max_length=16, verbose_name=_("Personal Password"), help_text=_("If you do not yet have a personal password, or if you have lost it, please contact the office."))

    def __int__(self):
        return self.patient_id


class Medication(models.Model):
    med_name = models.CharField(max_length=264, verbose_name=_("Drug name"), null=True, blank=True)
    med_dosage = models.FloatField(verbose_name=_("Dosage"), null=True, blank=True)
    patient = models.ForeignKey(Patient, on_delete=models.PROTECT)

    NONE = 0
    MG = 1
    G = 2
    ML = 3
    UNIT = (
        (NONE, _("")),
        (MG, _("mg")),
        (G, _("g")),
        (ML, _("ml")),

    )
    med_dosage_unit = models.PositiveSmallIntegerField(
        choices=UNIT, default=1, verbose_name=_("Unit"), null=True, blank=True
    )
    med_size = models.PositiveIntegerField(verbose_name=_("Drug amount per pkg"), null=True, blank=True)
    med_amount = models.PositiveIntegerField(verbose_name=_("How many?"), null=True, blank=True)

    ECARD = 1
    PRINT = 2
    DELIVER = (
        (ECARD, _("e-Card")),
        (PRINT, _("Pickup at the office")),
    )
    med_deliver = models.PositiveSmallIntegerField(
        choices=DELIVER, default=1, verbose_name=_("Delivery method")
    )
    created_at = models.DateTimeField(auto_now_add=True)
    status_closed = models.BooleanField(default=False, editable=True)

    def __str__(self):
        return self.med_name