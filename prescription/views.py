from django.shortcuts import render
from extra_views import InlineFormSetFactory, CreateWithInlinesView, NamedFormsetsMixin
from prescription.forms import MedForm, PatientForm
from prescription.models import *
from django.utils.translation import gettext_lazy as _


class MedicationInline(InlineFormSetFactory):
    model = Medication
    form_class = MedForm
    fields = "__all__"
    factory_kwargs = {
        'extra': 4,
        # 'min_num': 1,
        'max_num': 4,
        'can_order': False,
        'can_delete': True
    }
    # initial = [{'name': 'example1'}, {'name', 'example2'}]

class MedAddView(CreateWithInlinesView, NamedFormsetsMixin):
    template_name = "prescription/medication_form.html"
    model = Patient
    form_class = PatientForm
    inlines = [MedicationInline]
    inlines_names = ["medications"]
    success_url = '/order-sent/'

    def post(self, request, *args, **kwargs):
        return super().post(request, *args, **kwargs)