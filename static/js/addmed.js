window.onload=function(){
        let medForm = document.querySelector(".med-form")
        let container = document.querySelector("#form-container")
        let addButton = document.querySelector("#add-form")
        let totalForms = document.querySelector("#id_medication_set-TOTAL_FORMS")
        let delivery = document.querySelector("#id_medication_set-0-med_deliver")
        let formNum = medForm.length-1 // Get the number of the last form on the page with zero-based indexing

        // for (var i = 0; i < medForm.length; i += 1) {
        //         medForm.length-1;
        // }

function addForm(e) {

        e.preventDefault()
        let newForm = medForm[0].cloneNode(true) //Clone the med form

        let formRegex = RegExp(`form-(\\d){1}-`,'g') //Regex to find all instances of the form number
        formNum++ //Increment the form number

        newForm.innerHTML = newForm.innerHTML.replace(formRegex, `form-${formNum}-`) //Update the new form to have the correct form number
        container.insertBefore(newForm, addButton) //Insert the new form at the end of the list of forms
        delivery.style.visibility = 'hidden'
        totalForms.setAttribute('value', `${formNum+1}`) //Increment the number of total forms in the management form


}

addButton.addEventListener('click', addForm)
}