//script für Formatierung das aktuellen Tages in "Öffnungszeiten"
$(document).ready(function() {
  var weekday = new Date().getDay();
  // Mo - Fr
  if (weekday >= 1 && weekday <= 5) {
    $('.opening-time').eq(weekday-1).addClass('today');
  }
});


//Script für "Die Ordination hat derzeit Geschlossen/offen"
    const timeOpen = document.querySelectorAll('.time_open');
    const timeClosed = document.querySelectorAll('.time_closed');
    const openClosed = document.querySelector('.open_closed');

    const now = new Date();
    const hour = now.getHours();
    const minutes = now.getMinutes();
    const time = `${hour}:${minutes}`

    console.log(time);

    if(dateFns.isMonday(new Date(now))
        && time >= '08:00'
        && time <= '13:00'){
        openClosed.classList.add('open');
        openClosed.textContent = 'GEÖFFNET'
    }	else if(dateFns.isTuesday(new Date(now))
        && time >= '14:00'
        && time <= '17:00'){
        openClosed.classList.add('open');
        openClosed.textContent = 'GEÖFFNET'
      }
    else if(dateFns.isSaturday(new Date(now))
        && time >= '07:30'
        && time <= '12:30'){
        openClosed.classList.add('open');
        openClosed.textContent = 'GEÖFFNET'
    }
    else if(dateFns.isThursday(new Date(now))
        && time >= '15:00'
        && time <= '18:00'){
        openClosed.classList.add('open');
        openClosed.textContent = 'GEÖFFNET'
    }
    else if(dateFns.isFriday(new Date(now))
        && time >= '07:30'
        && time <= '11:30'){
        openClosed.classList.add('open');
        openClosed.textContent = 'GEÖFFNET'
    }
    else{
        openClosed.classList.add('closed');
        openClosed.textContent = 'GESCHLOSSEN'
    }
